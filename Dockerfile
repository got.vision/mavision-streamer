FROM nvcr.io/nvidia/deepstream-l4t:6.1-base

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update

RUN apt install python3-pip -y
RUN apt install libgstrtspserver-1.0-0 libgstreamer1.0-dev -y

WORKDIR /src

COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir --upgrade -r requirements.txt

COPY *.py ./

CMD ["python3", "main.py"]
