# Mavision Streamer
Will accept two gstreamer (and Deepstream) and stream them out

## Run
sudo docker run -it --rm --net=host --runtime nvidia -e DISPLAY=$DISPLAY -v /tmp/.X11-unix/:/tmp/.X11-unix nvcr.io/nvidia/l4t-base:r32.6.1

## License
Free for all. Watch out for upstream licences.

# Update
Can be updated to new versions from this base: [https://catalog.ngc.nvidia.com/orgs/nvidia/containers/deepstream/tags](https://catalog.ngc.nvidia.com/orgs/nvidia/containers/deepstream/tags)